package model;

import util.Util;

import java.io.IOException;

/**
 * Created by max on 31.07.21.
 */
public interface LocalStorageI {

	boolean load() throws IOException;
	// ------------------------------

	/**
	 * Set traffic provided last date.
	 * Последняя дата предоставления трафика (формат даты см в шаблоне {@link Util#DATE_PATTERN}).
	 *
	 * @param date date in String
	 * @see util.Util#DATE_PATTERN                // TODO: 31.07.21 may be move to LocalStorage class ?
	 */
	void setTrfProvidedLastDate(String date);

	/**
	 * Get traffic provided last date.
	 *
	 * @return date in string
	 * @see util.Util#DATE_PATTERN                // TODO: 31.07.21 may be move to LocalStorage class ?
	 */
	String getTrfProvidedLastDate();
	// ------------------------------

	/**
	 * Set traffic provided days.
	 * На сколько дней предоставляется траффик (дни), Пример: 28 дней.
	 *
	 * @param days amount of days
	 */
	void setTrfProvidedDays(int days);
	void setTrfProvidedDays(String days);

	/**
	 * Get traffic provided days.
	 *
	 * @return amount of days
	 */
	int getTrfProvidedDaysNum();
	String getTrfProvidedDaysStr();
	// ------------------------------

	/**
	 * Set traffic provided per plan period.
	 * Количество предоставляемого траффика (Байты).
	 *
	 * @param bytes traffic in Bytes
	 */
	void setTrfProvidedPerPlanPeriodBytes(long bytes);

	/**
	 * Set traffic provided per plan period.
	 *
	 * @param megaBytes traffic in MegaBytes
	 */
	void setTrfProvidedPerPlanPeriodMB(String megaBytes);

	/**
	 * Set traffic provided per plan period.
	 *
	 * @param gigaBytes traffic in GigaBytes
	 */
	void setTrfProvidedPerPlanPeriodGB(String gigaBytes);

	/**
	 * Get traffic provided per plan period.
	 *
	 * @return traffic in Bytes
	 */
	long getTrfProvidedPerPlanPeriodBytes();

	/**
	 * Get traffic provided per plan period.
	 *
	 * @return traffic in MegaBytes
	 */
	int getTrfProvidedPerPlanPeriodMB();
	String getTrfProvidedPerPlanPeriodMBStr();

	/**
	 * Get traffic provided per plan period.
	 *
	 * @return traffic in GigaBytes
	 */
	int getTrfProvidedPerPlanPeriodGB();
	String getTrfProvidedPerPlanPeriodGBStr();
	// ------------------------------


	/**
	 * Set traffic consumed. <br>
	 * Количество уже использованного траффика (Байты) <br>
	 * This method must be used for internal app calculations only!
	 *
	 * @param bytes traffic in Bytes
	 */
	void setTrfConsumedBytes(long bytes);

	/**
	 * Get traffic consumed. <br>
	 * This method must be used for internal app calculations only!
	 *
	 * @return traffic in Bytes
	 */
	long getTrfConsumedBytes();
	// ------------------------------


	// TODO: 31.07.21 correcting javadoc
	/**
	 * Set is auto reset traffic when time comes according terms of plan period. 	// TODO: 31.07.21 may be name  setTrf[Consumed]IsAutoReset ?
	 * Выполнять Авто-сброс траффика ?
	 *
	 * @param isAutoReset <tt>true</tt> auto reset traffic
	 */
	void setTrfIsAutoReset(boolean isAutoReset);

	/**
	 * Get auto reset traffic.
	 *
	 * @return <tt>true</tt> auto reset traffic
	 */
	boolean getTrfIsAutoReset();
	// ------------------------------


	/**
	 * Set is show numeric value of available traffic in Main Window.
	 * Показывать числовое значение доступного трафика ?
	 *
	 * @param isShowNumValue <tt>true</tt> show available traffic
	 */
	void setTrfAvlIsShowNumValue(boolean isShowNumValue);

	/**
	 * Get is show numeric value of available traffic in Main Window.
	 *
	 * @return <tt>true</tt> show available traffic
	 */
	boolean getTrfAvlIsShowNumValue();
	// ------------------------------

	/**
	 * Save data early set... methods to local storage.
	 */
	void save() throws IOException;
	// ------------------------------

}
