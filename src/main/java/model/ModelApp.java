package model;

import app.ConfigStorage;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pojo.TrafficStatistics;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by max on 09.04.21.
 */
public class ModelApp {

//	public ModelApp() {
//	}


	/**
	 * Connect to modem.
	 * @return true - connected, false - not connected
	 */
	public boolean connect() {
		String modemIp = (ConfigStorage.MODEM_IP);

		try {
			URL url = new URL(modemIp);
			URLConnection conn = url.openConnection();
			conn.connect();
			return true;
		} catch (Exception e) {
//			e.printStackTrace();
		}
		return false;
	}

	// Must be call async from thread
	public TrafficStatistics retrieveTrafficStatistics() {
		return retrieveTrafficStatisticsFromModem(ConfigStorage.MODEM_IP + ConfigStorage.ENDPOINT_TRAFFIC_STATISTICS);
	}

	// For testing and internal class call
	public TrafficStatistics retrieveTrafficStatisticsFromModem(String modemURL) {
		TrafficStatistics ts = null;

		try {
			URL url = new URL(modemURL);
			URLConnection urlConn = url.openConnection();
			ts = parseXML(urlConn.getInputStream());

//			NodeList currUplNodes = doc.getElementsByTagName("CurrentUpload");
//			NodeList currDlNodes = doc.getElementsByTagName("CurrentDownload");
//
//			for(int i=0; i<currUplNodes.getLength();i++) {
//				i1 = currUplNodes.item(i).getTextContent();
//			}
//
//			for(int i=0; i<currDlNodes.getLength();i++) {
//				i2 = currDlNodes.item(i).getTextContent();
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ts;
	}

//	public pojo._Traffic parseTraffic(Document doc) {
//		String s1 = "x", s2 = "x";
//
//		NodeList currUplNodes = doc.getElementsByTagName("CurrentUpload");
//		NodeList currDlNodes = doc.getElementsByTagName("CurrentDownload");
//
//		for(int i=0; i<currUplNodes.getLength();i++) {
//			s1 = currUplNodes.item(i).getTextContent();
//		}
//
//		for(int i=0; i<currDlNodes.getLength();i++) {
//			s2 = currDlNodes.item(i).getTextContent();
//		}
//
//		System.out.println("" + s1 + " | " + s2);
//		return new pojo._Traffic(1, 2);
//	}

//	private Document parseXML(InputStream stream) throws Exception {
//		DocumentBuilderFactory objDocumentBuilderFactory;
//		DocumentBuilder objDocumentBuilder;
//		Document doc = null;
//		try	{
//			objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
//			objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();
//			doc = objDocumentBuilder.parse(stream);
//		}
//		catch(IOException | ParserConfigurationException e){
//			e.printStackTrace();
//		}
//		return doc;
//	}


	// Parse pojo.TrafficStatistics XML
	// https://java-course.ru/begin/xml/
	public TrafficStatistics parseXML(InputStream is) {    // TODO: 24.07.21 removed comments
		TrafficStatistics tStat = new TrafficStatistics();

		try {
			// Создается построитель документа
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			// Создается дерево DOM документа из файла
			Document doc = documentBuilder.parse(is);

			// Получаем корневой элемент
			Node root = doc.getDocumentElement();

//			System.out.println("List of rsp:");
//			System.out.println();
			// Просматриваем все подэлементы корневого - т.е. книги
			NodeList rsp = root.getChildNodes();
//			System.out.println("@ " + rsp.getLength());
			for (int i = 0; i < rsp.getLength(); i++) {
				Node rspProp = rsp.item(i);
//				System.out.println("# " + rspProp.getNodeName());
				if (rspProp.getNodeType() != Node.TEXT_NODE) {
//					System.out.println("## " + rspProp.getNodeName() + " : " + rspProp.getChildNodes().item(0).getTextContent());
					String valStr = rspProp.getChildNodes().item(0).getTextContent();
					switch (rspProp.getNodeName()) {
						case "CurrentConnectTime":
							tStat.setCurrentConnectTimeStr(valStr);
							break;
						case "CurrentUpload":
							tStat.setCurrentUploadStr(valStr);
							break;
						case "CurrentDownload":
							tStat.setCurrentDownloadStr(valStr);
							break;
						case "TotalConnectTime":
							tStat.setTotalConnectTimeStr(valStr);
							break;
					}
				}
			}

		} catch (ParserConfigurationException | SAXException | IOException ex) {
			ex.printStackTrace(System.out);
		}

		return tStat;
	}
}
