package model;

import util.AppProperties;
import util.Util;

import java.io.IOException;


public class LocalStorageAdapter implements LocalStorageI {

	@Override
	public boolean load() throws IOException {
		return AppProperties.load();
	}
	// ------------------------------

	@Override
	public void setTrfProvidedLastDate(String date) {
		AppProperties.set(AppProperties.Options.TRAFFIC_PROVIDED_LAST_DATE, date);
	}

	@Override
	public String getTrfProvidedLastDate() {
		return AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_LAST_DATE, "-- -- ----");
	}
	// ------------------------------

	@Override
	public void setTrfProvidedDays(int days) {
		AppProperties.set(AppProperties.Options.TRAFFIC_PROVIDED_DAYS, String.valueOf(days));
	}

	@Override
	public void setTrfProvidedDays(String days) {
		AppProperties.set(AppProperties.Options.TRAFFIC_PROVIDED_DAYS, days);
	}

	@Override
	public int getTrfProvidedDaysNum() {
		return Integer.parseInt( AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_DAYS, "0") );    // TODO: 31.07.21 default vale 0 is correct ?
	}

	@Override
	public String getTrfProvidedDaysStr() {
		return AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_DAYS, "----");
	}

	// ------------------------------

	@Override
	public void setTrfProvidedPerPlanPeriodBytes(long bytes) {
		AppProperties.set(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, String.valueOf(bytes));
	}

	@Override
	public void setTrfProvidedPerPlanPeriodMB(String megaBytes) {
		long bytes = Util.convertMBToBytes(Integer.parseInt(megaBytes));
		AppProperties.set(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, String.valueOf(bytes));
	}

	@Override
	public void setTrfProvidedPerPlanPeriodGB(String gigaBytes) {
		// ...
	}

	@Override
	public long getTrfProvidedPerPlanPeriodBytes() {
		String bytesStr = AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, "0");
		return Long.parseLong(bytesStr);
	}

	@Override
	public int getTrfProvidedPerPlanPeriodMB() {
		String bytesStr = AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, "0");
		return (int) (Long.parseLong(bytesStr) / 1024 / 1024);
	}

	@Override
	public String getTrfProvidedPerPlanPeriodMBStr() {
		String bytesStr = AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, "0");
		int mb = (int) (Long.parseLong(bytesStr) / 1024 / 1024);
		return String.valueOf(mb);
	}

	@Override
	public int getTrfProvidedPerPlanPeriodGB() {
		String bytesStr = AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, "0");
		return (int) (Long.parseLong(bytesStr) / 1024 / 1024 / 1024);
	}

	@Override
	public String getTrfProvidedPerPlanPeriodGBStr() {
		String bytesStr = AppProperties.get(AppProperties.Options.TRAFFIC_PROVIDED_PER_PLAN_PERIOD, "0");
		int mb = (int) (Long.parseLong(bytesStr) / 1024 / 1024 / 1024);
		return String.valueOf(mb);
	}

	// ------------------------------

	@Override
	public void setTrfConsumedBytes(long bytes) {
		AppProperties.set(AppProperties.Options.TRAFFIC_CONSUMED_INTERNAL, String.valueOf(bytes));
	}

	@Override
	public long getTrfConsumedBytes() {
		String bytesStr = AppProperties.get(AppProperties.Options.TRAFFIC_CONSUMED_INTERNAL, "0");
		return Long.parseLong(bytesStr);
	}
	// ------------------------------

	@Override
	public void setTrfIsAutoReset(boolean isAutoReset) {
		AppProperties.set(AppProperties.Options.TRAFFIC_IS_AUTO_RESET, AppProperties.boolPrimitiveToStr(isAutoReset));
	}

	@Override
	public boolean getTrfIsAutoReset() {
		return AppProperties.boolStrToPrimitive( AppProperties.get(AppProperties.Options.TRAFFIC_IS_AUTO_RESET, AppProperties.Options.TRUE) );
	}
	// ------------------------------

	@Override
	public void setTrfAvlIsShowNumValue(boolean isShowNumValue) {
		AppProperties.set(AppProperties.Options.TRAFFIC_IS_SHOW_NUM_VAL, AppProperties.boolPrimitiveToStr(isShowNumValue));
	}

	@Override
	public boolean getTrfAvlIsShowNumValue() {
		return AppProperties.boolStrToPrimitive(AppProperties.get(AppProperties.Options.TRAFFIC_IS_SHOW_NUM_VAL, AppProperties.Options.TRUE));
	}
	// ------------------------------


	@Override
	public void save() throws IOException {
		AppProperties.save();
	}
	// ------------------------------

}
