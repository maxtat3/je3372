package controller;

import model.LocalStorageI;
import util.Util;
import view.ViewSettings;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Controller for {@link ViewSettings}
 */
public class ControllerSettings {        // TODO: 31.07.21 this controller must be create view instance !

	private LocalStorageI storage;
	private ViewSettings.SettingsTransfer settingsTransfer;


	public ControllerSettings() {
	}

	public ControllerSettings(LocalStorageI storage) {
		this.storage = storage;
	}

	public ControllerSettings(ViewSettings.SettingsTransfer settingsTransfer) {
		this.settingsTransfer = settingsTransfer;
	}

	public void setSettingsTransfer(ViewSettings.SettingsTransfer settingsTransfer) {
		this.settingsTransfer = settingsTransfer;
	}


	public ViewSettings.SettingsTransfer readSettings() {
		ViewSettings.SettingsTransfer st = new ViewSettings().new SettingsTransfer();    // TODO: 31.07.21 may be use class variable ?

		st.trfProvidedLastDate = storage.getTrfProvidedLastDate();
		st.trfProvidedDays = storage.getTrfProvidedDaysStr();
		st.trfChargePerPlanPeriod = storage.getTrfProvidedPerPlanPeriodMBStr();
		st.trfIsAutoReset = storage.getTrfIsAutoReset();
		st.trfIsShowNumVal = storage.getTrfAvlIsShowNumValue();

		return st;
	}

	/**
	 * Save settings to local storage.
	 *
	 * @return <tt>true</tt> is save success
	 */
	public ValidationResult save() {
		ValidationResult vRes;

		// --------------------
		// 	Check user input
		// --------------------
		String trfProvidedLastDateStr = settingsTransfer.trfProvidedLastDate;
		vRes = checkInputDateFormat(trfProvidedLastDateStr, "0");
		if( !vRes.isSuccess() ) return vRes;

		String trfProvidedDaysStr = settingsTransfer.trfProvidedDays;    // TODO: 29.07.21 may be less names ?
		vRes = checkInputNumericValue(trfProvidedDaysStr, "1", 1, 366);
		if( !vRes.isSuccess() ) return vRes;

		String trfChargePerPlanPeriodMBStr = settingsTransfer.trfChargePerPlanPeriod;
		vRes = checkInputNumericValue(trfChargePerPlanPeriodMBStr, "2", 1, Integer.MAX_VALUE);
		if ( !vRes.isSuccess() ) return vRes;
		// end check user input
		// --------------------

		// -----------------------------
		// 	Save data to local storage
		// -----------------------------
		try {
			storage.setTrfProvidedLastDate(trfProvidedLastDateStr);
			storage.setTrfProvidedDays(trfProvidedDaysStr);
			storage.setTrfProvidedPerPlanPeriodMB(trfChargePerPlanPeriodMBStr);
			storage.setTrfIsAutoReset(settingsTransfer.trfIsAutoReset);
			storage.setTrfAvlIsShowNumValue(settingsTransfer.trfIsShowNumVal);

			storage.save();
		} catch (IOException e) {
			e.printStackTrace();
			return new ValidationResult(false, "Ошибка записи");
		}
		// -----------------------------

		return new ValidationResult(true);
	}

	/**
	 * Check input numeric value in text field.
	 *
	 * @param numStr string presentation of numeric
	 * @param input name of checked input form
	 * @param l left range
	 * @param r right range
	 * @return object ValidationResult with true value - if check is correct, otherwise object ValidationResult with false value and explain message
	 */
	public ValidationResult checkInputNumericValue(String numStr, String input, int l, int r) {
		if ( !Util.isNumeric(numStr) ) {
			return new ValidationResult(false, "В поле [" + input + "] должно быть число!");
		}

		int val = Integer.parseInt(numStr);
		if (val <= l || val > r) {
			return new ValidationResult(false, "В поле [" + input + "] допускается диапазон значений от " + l + " до " + r + ".");
		}

		return new ValidationResult(true);
	}

	/**
	 * Check input date format. Correct date format see in pattern {@link Util#DATE_PATTERN}
	 *
	 * @param date input date
	 * @return object ValidationResult with true value - if check is correct, otherwise object ValidationResult with false value and explain message
	 */
	public ValidationResult checkInputDateFormat(String date, String input) {
		DateFormat sdf = new SimpleDateFormat(Util.DATE_PATTERN);
		sdf.setLenient(false);
		try {
			sdf.parse(date);
		} catch (ParseException e) {
			return new ValidationResult(false, "В поле [" + input + "] дата должна соответствовать шаблону " + Util.DATE_PATTERN);
		}

		return new ValidationResult(true);
	}


	public class ValidationResult {
		private boolean isSuccess;
		private String message;

		public ValidationResult(boolean isSuccess) {
			this.isSuccess = isSuccess;
		}

		public ValidationResult(boolean isSuccess, String message) {
			this.isSuccess = isSuccess;
			this.message = message;
		}

		public boolean isSuccess() {
			return isSuccess;
		}

		public String getMessage() {
			return message;
		}
	}

	public void resetConsumedTrafficManual() {
		try {
			storage.setTrfConsumedBytes(0);
			storage.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

