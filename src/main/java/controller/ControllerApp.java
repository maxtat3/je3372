package controller;

import model.LocalStorageI;
import model.ModelApp;
import pojo.TrafficStatistics;
import util.Util;
import view.ViewApp;
import view.ViewAppCallbackI;

import java.io.IOException;
import java.text.ParseException;

public class ControllerApp implements ModemConnectorThreadCallbackI, ModemReceiverThreadCallbackI {

	private static final boolean IS_LOG = true;

	private ViewAppCallbackI viewCallback;
	private LocalStorageI storage;
	private ModelApp modelApp;

	private ConnState connState = ConnState.DISCONNECTED;


	/**
	 * Constructor for unit  test only.
	 *
	 * @param storage
	 */
	public ControllerApp(LocalStorageI storage) {
		this.storage = storage;
	}

	public ControllerApp(ViewAppCallbackI viewCallback, LocalStorageI localStorage) {
		this.viewCallback = viewCallback;
		this.storage = localStorage;

//		if(isNewTrfAvailable(Util.getCurrentDate())) makeTrfProvidedPerPlanPeriod();    // TODO: 07.08.21 redundant method

		viewCallback.setFontSize(ViewApp.FontSize.MEDIUM);    // TODO: 22.07.21  must be retrieve from settings

		modelApp = new ModelApp();

		Thread modemConnThd = new Thread(new ModemConnectorThread("Thread conn to modem", this));
		modemConnThd.start();

		Thread modemRxThd = new Thread(new ModemReceiverThread("Thread modem RX traffic", this));
		modemRxThd.start();
	}


	/**
	 * Check is new traffic available provided per plan period.
	 * Before call this method must be load app settings from call method localStorage.load();
	 *
	 * @param currentDate current date in string representation according to {@link Util#DATE_PATTERN} pattern
	 * @return <tt>true</tt> if available new traffic
	 */
	public boolean isNewTrfAvailable(String currentDate) {
		// Amount of days to provide new traffic. If == 0 or < 0 new traffic is available.
		// Set to 1 because by default traffic not provide
		long amountDaysNewTrf = 1;

		try {
			// Example args in Util.addDays(...) method: 1 arg: (str) 06 07 2021, 2 arg: (int) 28
			String nextDate = Util.addDays(storage.getTrfProvidedLastDate(), storage.getTrfProvidedDaysNum());
			amountDaysNewTrf = Util.getDifferenceDays(currentDate, nextDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return amountDaysNewTrf == 0 || amountDaysNewTrf < 0;
	}

//	/**
//	 * Make changes to local storage according to which new traffic is available.
//	 */
//	private void makeTrfProvidedPerPlanPeriod() {
//		System.out.println("+");
////		try {
////			storage.setTrfConsumedBytes(0);
////			storage.setTrfProvidedLastDate(Util.getCurrentDate());
////			storage.save();
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
//	}


	/**
	 * Thread for connection to modem
	 */
	private class ModemConnectorThread implements Runnable {
		public static final int REFRESH_TIME_MS = 1500;	// refresh connect timeout

		// true - suspended thread
		private volatile boolean suspended;

		// true - stop thread
		private volatile boolean stopped;

		private final String name;
		private final ModemConnectorThreadCallbackI callback;


		public ModemConnectorThread(String name, ModemConnectorThreadCallbackI callback) {
			this.name = name;
			this.callback = callback;
			suspended = false;
			stopped = false;
		}

		@Override
		public void run() {
			log("Started try connection to modem ...");
			try {
				while (!stopped){
					if (!modelApp.connect()) {
						if (connState == ConnState.CONNECTED) {
							callback.setConnState(ConnState.DISCONNECTED);
							Thread.sleep(REFRESH_TIME_MS * 2);
						}
						log("conn attempt ...");
						callback.setConnState(ConnState.TRY_CONNECT);

					} else {
						log("Connected !");
						callback.setConnState(ConnState.CONNECTED);
					}
					Thread.sleep(REFRESH_TIME_MS);

					synchronized (this) {
						while (suspended) {
							wait();
						}
						if (stopped) break;
					}
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		synchronized void stop() {
			log("Stop thread.");
			stopped = true;
			suspended = false;
			notify();
		}

		synchronized void suspend() {
			suspended = true;
		}

		synchronized void resume() {
			suspended = false;
			notify();
		}

		private void log(String msg) {
			System.out.println(name + ": " + msg);
		}
	}

	/**
	 * Thread for receive data (traffic) from modem
	 */
	private class ModemReceiverThread implements Runnable {
		public static final int REFRESH_TIME_MS = 3000;

		private String name;
		private ModemReceiverThreadCallbackI callback;


		public ModemReceiverThread(String name, ModemReceiverThreadCallbackI callback) {
			this.name = name;
			this.callback = callback;
		}

		@Override
		public void run() {
			while (true) {    // FIXME: 24.07.21 may be throw exception in while
				if (connState == ConnState.CONNECTED) {
					TrafficStatistics tStat = modelApp.retrieveTrafficStatistics();
					callback.updateTraffic(tStat);
				}
				try {
					Thread.sleep(REFRESH_TIME_MS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}


	@Override
	public void setConnState(ConnState connState) {
		this.connState = connState;
		viewCallback.setConnectionState(connState);
	}


	private boolean is1stBuf = true;	// selector for temporary store 1-st (prev) or 2-nd (curr) buffer.
	private long prev, curr;

	@Override
	public void updateTraffic(TrafficStatistics tStat) {
		long trDlUplBytes = tStat.getCurrentDownload() + tStat.getCurrentUpload();

		// update traffic in local storage
		if (is1stBuf) {
			prev = trDlUplBytes;

		} else {
			curr = trDlUplBytes;
			long diff = curr - prev;
			long consumedTrfBytes = storage.getTrfConsumedBytes();
			consumedTrfBytes += diff;
			log("save | diff= " + diff + " | consumedTrfBytes = " + consumedTrfBytes);
			storage.setTrfConsumedBytes(consumedTrfBytes);    // TODO: 04.08.21 must be save to local storage every 0.5 or 1 minute !
			try {
				storage.save();        // TODO: 31.07.21 may be moved exception to interface ?
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		is1stBuf = !is1stBuf;

		long trfAvBytes = storage.getTrfProvidedPerPlanPeriodBytes() - storage.getTrfConsumedBytes();
		long trfPlanPr = storage.getTrfProvidedPerPlanPeriodBytes();
		int trfAvPercentage = (int)( ((double)trfAvBytes / trfPlanPr) * 100);
		viewCallback.setTrfAvailableMB( Util.convertBytesToMB(trfAvBytes) );
		viewCallback.setTrfAvailablePercentage(trfAvPercentage);
	}


	private static void log(String msg) {
		if(IS_LOG) System.out.println(ControllerApp.class.getSimpleName() + ": " + msg);
	}
}
