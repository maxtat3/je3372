package controller;

/**
 * Created by max on 22.07.21.
 */
public enum ConnState {
	DISCONNECTED("Модем отключен", "#808080"),
	TRY_CONNECT("Подключение...", "#ff9900"),
	CONNECTED("Модем подключен", "#33cc33");

	private String state;
	private String textColor;

	ConnState(String state, String textColor) {
		this.state = state;
		this.textColor = textColor;
	}

	public String getState() {
		return state;
	}

	public String getTextColor() {
		return textColor;
	}
}
