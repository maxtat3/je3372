package controller;

import controller.ConnState;

/**
 * Callback interface for ModemConnectorThread class
 */
public interface ModemConnectorThreadCallbackI {

	void setConnState(ConnState connState);

}
