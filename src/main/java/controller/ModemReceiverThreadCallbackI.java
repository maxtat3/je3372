package controller;

import pojo.TrafficStatistics;

public interface ModemReceiverThreadCallbackI {

	void updateTraffic(TrafficStatistics trafficStatistics);

}
