package util;

import java.io.*;
import java.util.Properties;

/**
 * Load, save, update configuration of this app in property file in local.
 */
public class AppProperties {

	private static final boolean IS_LOG = true;

	public class Options {
		/** Последняя дата предоставления трафика (формат даты см в шаблоне {@link Util#DATE_PATTERN}) */
		public static final String TRAFFIC_PROVIDED_LAST_DATE = "traffic.provided-last-date";

		/** На сколько дней предоставляется траффик (дни), Пример: 28 дней */
		public static final String TRAFFIC_PROVIDED_DAYS  = "traffic.provided-days";

		/** Количество предоставляемого траффика (Байты) */
		public static final String TRAFFIC_PROVIDED_PER_PLAN_PERIOD = "traffic.charge-per-plan-period";

		/** Количество уже использованного траффика (Байты) */
		public static final String TRAFFIC_CONSUMED_INTERNAL = "traffic.internal.consumed";    // TODO: 27.07.21 Подумать над правильностью названия настройки

		/** Авто-сброс траффика (boolean) */
		public static final String TRAFFIC_IS_AUTO_RESET = "traffic.auto-reset";

		/** Показывать числовое значение трафика в главном окне (boolean) */
		public static final String TRAFFIC_IS_SHOW_NUM_VAL = "traffic.show-num-val";

		/** String representation <tt>true</tt> default value **/
		public static final String TRUE = "true";
		/** String representation <tt>false</tt> default value **/
		public static final String FALSE = "false";
	}

	private static final String SYS_PROP_USR_HOME = System.getProperty("user.home");
	private static final String SYS_PROP_FILE_SEP = System.getProperty("file.separator");

	private static final String PROP_FILE_NAME = "jE3372.properties";
	private static final String PROP_FILE = SYS_PROP_USR_HOME + SYS_PROP_FILE_SEP + PROP_FILE_NAME;
	private static Properties props;
	private static String propFilePath;

	// This method must be call first in main method.
	// If prop file not created before (first run app) - will be forming path and if next call save() method it saved data from this path.
	// Otherwise throwing Exception !
	public static boolean load() throws IOException {
		boolean isLoadFile;

		if(!System.getProperties().containsKey(PROP_FILE_NAME)) {
			log("Not find Property file !!!");

			if(OSUtils.isLinux()) {
				String configBase = System.getenv("XDG_CONFIG_HOME");
				if(null == configBase || configBase.trim().equals("")) {
					log("Not find XDG_CONFIG_HOME");
					configBase = SYS_PROP_USR_HOME + SYS_PROP_FILE_SEP + ".config";
				}
				System.setProperty(PROP_FILE_NAME, configBase + SYS_PROP_FILE_SEP + PROP_FILE_NAME);
			}
			else if(OSUtils.isMacOSX()) {
				System.setProperty(PROP_FILE_NAME, SYS_PROP_USR_HOME + SYS_PROP_FILE_SEP + "Library" + SYS_PROP_FILE_SEP + "Preferences" + SYS_PROP_FILE_SEP + PROP_FILE_NAME);
			}
		}

		propFilePath = System.getProperty(PROP_FILE_NAME);
		if (propFilePath == null || propFilePath.trim().equals("")) {
			log("Not find Property file, set it to: " + PROP_FILE);
			propFilePath = PROP_FILE;
		}

		File prefs = new File(propFilePath);
		prefs.getParentFile().mkdirs();

		//Attempt to load the properties
		try {
			log("Loading the Properties file [" + propFilePath + "]");
			props = new Properties();
			props.load(new FileInputStream(propFilePath));
			isLoadFile = true;
		} catch (FileNotFoundException e) {
			isLoadFile = false;
			log("Property file not found. Will be created the next time the properties are saved.");
		}

		return isLoadFile;
	}


	public static String get(String name, String defaultValue) {
		String retVal = props.getProperty(name, defaultValue);
		log("Returning the Property, name=" + name + ", value=" + retVal);
		return retVal;
	}

	public static String get(String name) {
		return get(name, null);
	}

	public static int getInt(String name, int defaultValue) {
		String cfgVal = props.getProperty(name);
		int ret = defaultValue;
		if (cfgVal != null && Util.isNumeric(cfgVal)) {
			ret = Integer.parseInt(cfgVal);
		}
		log("Returning the property, name=" + name + ", value=" + ret);
		return ret;
	}

	public static long getLong(String name, long defaultValue) {
		String cfgVal = props.getProperty(name);
		long ret = defaultValue;
		if (cfgVal != null && Util.isNumeric(cfgVal)) {
			ret = Long.parseLong(cfgVal);
		}
		log("Returning the property, name=" + name + ", value=" + ret);
		return ret;
	}

	/**
	 * Set option in property. Options must take from {@link Options} internal class.
	 *
	 * @param name option name
	 * @param value option value
	 */
	public static void set(String name, String value) {
		log("Setting the property, name=" + name + ", value=" + value);
		props.setProperty(name, value);
	}

	public static void save() throws IOException {
		log("Saving properties to the file [" + propFilePath + "]");
		props.store(new FileOutputStream(propFilePath), "E3372 monitor");
	}


	/**
	 * Convert primitive boolean type to string representation.
	 * Example:
	 *
	 * @param isSet
	 * @return
	 * @see Options#TRUE
	 * @see Options#FALSE
	 */
	public static String boolPrimitiveToStr(boolean isSet) {
		return (isSet ? Options.TRUE : Options.FALSE);
	}

	/**
	 * Convert string representation boolean to primitive type. 
	 *
	 * @param isSet
	 * @return
	 * @see Options#TRUE
	 * @see Options#FALSE
	 */
	public static boolean boolStrToPrimitive(String isSet) {
		switch (isSet) {
			case Options.TRUE:
				return true;

			case Options.FALSE:
				return false;

			default:
				throw new IllegalArgumentException("In parameter method must be " + Options.TRUE + " or " + Options.FALSE + " values");
		}
	}


	private static void log(String msg) {
		if(IS_LOG) System.out.println(AppProperties.class.getSimpleName() + ": " + msg);
	}

}
