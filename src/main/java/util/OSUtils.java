package util;

/**
 * Helper OS definition functions.
 */
public class OSUtils {

	public static boolean isLinux(){
		return System.getProperty("os.name").equals("Linux");
	}

	public static boolean isWindows(){
		return System.getProperty("os.name").startsWith("Windows");
	}

	public static boolean isMacOSX(){
		return System.getProperty("os.name").equals("Mac OS X");
	}

}
