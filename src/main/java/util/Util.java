package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Util {

	public static final String DATE_PATTERN = "dd MM yyyy";
	/**
	 * Convert Bytes to MegaBytes (MB)
	 *
	 * @param bytes input in Bytes
	 * @return MegaBytes
	 */
	public static int convertBytesToMB(long bytes) {
		float mb = (float) (bytes / 1024.0 / 1024.0);
		return Math.round(mb);
	}

	/**
	 * Convert MegaBytes (MB) to Bytes
	 *
	 * @param megaBytes input in MegaBytes
	 * @return Bytes
	 */
	public static long convertMBToBytes(int megaBytes) {
		return (long)megaBytes * 1024L * 1024L;
	}

	/**
	 * Calculating days between two dates.
	 *
	 * @param d1 first date from which you need to count, example 15 05 2021
	 * @param d2 second date to which to count down, example 17 05 2021
	 * @return difference date in days
	 * @throws ParseException           when passed date format is incorrect
	 * @throws IllegalArgumentException when date 1 larger date 2
	 */
	public static long getDifferenceDays(String d1, String d2) throws ParseException {
		SimpleDateFormat myFormat = new SimpleDateFormat(DATE_PATTERN);
		Date date1 = myFormat.parse(d1);
		Date date2 = myFormat.parse(d2);
		long diff = date2.getTime() - date1.getTime();
//		if (diff < 0) throw new IllegalArgumentException("Date " + d2 + " must be larger when " + d1);	// TODO: 04.08.21 int value instead IllegalArgumentException
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	/**
	 * Add days to date.
	 *
	 * @param date date from which you need to add
	 * @param days how much days will be added
	 * @return new date in string representation according to {@link #DATE_PATTERN} pattern
	 */
	public static String addDays(String date, int days) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(date));
		c.add(Calendar.DATE, days);
		return sdf.format(c.getTime());
	}

	/**
	 * Get current calendar date.
	 *
	 * @return current date in string representation according to {@link #DATE_PATTERN} pattern
	 */
	public static String getCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(Util.DATE_PATTERN);
		Calendar c = Calendar.getInstance();
		return sdf.format(c.getTime());
	}

	/**
	 * Checks whether the String a valid numeric.
	 *
	 * @param s <tt>String</tt> to check
	 * @return <tt>true</tt> if the string is a correctly formatted number
	 */
	public static boolean isNumeric(String s) {
		try {
			Long.parseLong(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	// Also may be apply next method instead above.
//	public static boolean isNumeric(String str){
//		NumberFormat formatter = NumberFormat.getInstance();
//		ParsePosition pos = new ParsePosition(0);
//		formatter.parse(str, pos);
//		return str.length() == pos.getIndex();
//	}
}
