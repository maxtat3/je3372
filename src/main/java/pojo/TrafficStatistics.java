package pojo;

/**
 * Created by max on 23.07.21.
 */
public class TrafficStatistics {

	private String currentUploadStr;
	private String currentDownloadStr;
	private String currentConnectTimeStr;
	private String totalConnectTimeStr;


	public void setCurrentUploadStr(String currentUploadStr) {
		this.currentUploadStr = currentUploadStr;
	}

	public void setCurrentDownloadStr(String currentDownloadStr) {
		this.currentDownloadStr = currentDownloadStr;
	}

	public void setCurrentConnectTimeStr(String currentConnectTimeStr) {
		this.currentConnectTimeStr = currentConnectTimeStr;
	}

	public void setTotalConnectTimeStr(String totalConnectTimeStr) {
		this.totalConnectTimeStr = totalConnectTimeStr;
	}


	public long getCurrentUpload() {
		return convertStrNumToLong(currentUploadStr);
	}

	public long getCurrentDownload() {
		return convertStrNumToLong(currentDownloadStr);
	}

	public long getCurrentConnectTime() {
		return convertStrNumToLong(currentConnectTimeStr);
	}

	public long getTotalConnectTime() {
		return convertStrNumToLong(totalConnectTimeStr);
	}


	/**
	 * Convert number value presented as string to long.
	 *
	 * @param strNumber number value presented as string
	 * @return long number value result
	 */
	private long convertStrNumToLong(String strNumber) {
		Long num = -1L;
		try {
			num = Long.valueOf(strNumber);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return num;
	}

}
