package view;

import controller.ConnState;

/**
 * Created by max on 10.04.21.
 */
public interface ViewAppCallbackI {

	/**
	 * Set modem connection state.
	 *
	 * @param state connection state
	 * @see ConnState
	 */
	void setConnectionState(ConnState state);

	/**
	 * Set available traffic in MB in main window.
	 *
	 * @param traffic traffic in MB
	 */
	void setTrfAvailableMB(int traffic);

	/**
	 * Set available traffic in MB in main window.
	 *
	 * @param percentage traffic in percentage in range [0 ... 100]
	 */
	void setTrfAvailablePercentage(int percentage);

	/**
	 * Set font size all text elements in main window.
	 *
	 * @param size size in px
	 */
	void setFontSize(ViewApp.FontSize size);
}
