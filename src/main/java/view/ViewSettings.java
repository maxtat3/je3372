package view;

import controller.ControllerSettings;
import model.LocalStorageI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by max on 10.04.21.
 */
public class ViewSettings extends JDialog {

	private JTextField jtfTrfProvidedLastDate;
	private JTextField jtfTrfProvidedDays;
	private JTextField jtfTrfProvidedPerPlanPeriod;
	private JCheckBox jchbTrfAutoReset;
	private JCheckBox jchbTrfIsShowNumVal;

	private ControllerSettings cs;


	// This constructor for create SettingTransfer inner class
	public ViewSettings() {
	}

	public ViewSettings(JFrame jFrame, LocalStorageI localStorage) {
		super(jFrame, "Настройки", true);
		cs = new ControllerSettings(localStorage);

		createAndShowGUI();
		readSettings();
	}


	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private void createAndShowGUI() {
		Container container = getContentPane();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

		JPanel jpTrf = new JPanel();
		jpTrf.setLayout(new GridBagLayout());
//		jpTrf.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 1, true), "Трафик"));
		jpTrf.setBorder(BorderFactory.createTitledBorder("Трафик"));
		GridBagConstraints gbc = new GridBagConstraints();

		JPanel jpConfirmBtn = new JPanel();
		jpConfirmBtn.setLayout(new BoxLayout(jpConfirmBtn, BoxLayout.X_AXIS));

		// По умолчанию натуральная высота, максимальная ширина
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.gridy   = 0;

		int row = 0;

		JLabel jl0a = new JLabel("Последняя дата предоставления трафика (dd MM yyyy): ");
		gbc.gridy = row;		// нулевая ячейка таблицы по вертикали
		gbc.gridx = 0;			// нулевая ячейка таблицы по горизонтали
		jpTrf.add(jl0a, gbc);

		jtfTrfProvidedLastDate = new JTextField("-- -- ----", 8);
		gbc.gridy = row;
		gbc.gridx = 1;
		jpTrf.add(jtfTrfProvidedLastDate, gbc);

		row++;
		// -------

		JLabel jl0 = new JLabel("На сколько дней предоставляется трафик: ");
		gbc.gridy = row;
		gbc.gridx = 0;
		jpTrf.add(jl0, gbc);

		jtfTrfProvidedDays = new JTextField("--", 4);
		gbc.gridy = row;
		gbc.gridx = 1;
		jpTrf.add(jtfTrfProvidedDays, gbc);

		row++;
		// -------

		JLabel jl1 = new JLabel("Количество предоставляемого трафика (МБ): ");
		gbc.gridy = row;
		gbc.gridx = 0;
		jpTrf.add(jl1, gbc);

		jtfTrfProvidedPerPlanPeriod = new JTextField("---", 5);
		gbc.gridy = row;
		gbc.gridx = 1;
		jpTrf.add(jtfTrfProvidedPerPlanPeriod, gbc);

		row++;
		// -------

		jchbTrfAutoReset = new JCheckBox("Авто сброс трафика", false);
		gbc.gridy = row;
		gbc.gridx = 0;
		jpTrf.add(jchbTrfAutoReset, gbc);

		row++;
		// -------

		JButton jbtnTrfManualReset = new JButton("Сбросить трфик вручную");
		jbtnTrfManualReset.addActionListener(new ActionListener() {    // FIXME: 29.07.21 дописать Action
			@Override
			public void actionPerformed(ActionEvent e) {
				int answ = JOptionPane.showConfirmDialog(getContentPane(), "Действительно выполнить сброс ?", "Ручной сброс трафика", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (answ == JOptionPane.YES_OPTION) {
					cs.resetConsumedTrafficManual();
				}
			}
		});
		gbc.gridy = row;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		jpTrf.add(jbtnTrfManualReset, gbc);
		row++;
		// -------

		jchbTrfIsShowNumVal = new JCheckBox("Показывать числовое представление трафика", false);
		gbc.gridy = row;
		gbc.gridx = 0;
		jpTrf.add(jchbTrfIsShowNumVal, gbc);
		row++;
		// -------

		JButton jbtnOk = new JButton("Ok");
		jbtnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsTransfer settings = new SettingsTransfer();
				settings.trfProvidedLastDate = jtfTrfProvidedLastDate.getText();
				settings.trfProvidedDays = jtfTrfProvidedDays.getText();
				settings.trfChargePerPlanPeriod = jtfTrfProvidedPerPlanPeriod.getText();
				settings.trfIsAutoReset = jchbTrfAutoReset.isSelected();
				settings.trfIsShowNumVal = jchbTrfIsShowNumVal.isSelected();

				boolean ret = okClicked(settings);
				if (ret) dispose();
			}
		});
		gbc.gridy = row;
		gbc.gridx = 1;
		gbc.gridwidth = 1;
//		gbc.anchor = GridBagConstraints.CENTER;
		jpConfirmBtn.add(jbtnOk, gbc);

		JButton jbtnCancel = new JButton("Cancel");
		jbtnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Close Settings dialog");
				dispose();
			}
		});
		gbc.gridy = row;
		gbc.gridx = 2;
		gbc.gridwidth = 1;
//		gbc.anchor = GridBagConstraints.CENTER;
		jpConfirmBtn.add(jbtnCancel, gbc);

		row++;
		// -------

		container.add(jpTrf);
		container.add(jpConfirmBtn);
	}

	/**
	 * Read settings from local storage and show in view.
	 */
	private void readSettings() {
		SettingsTransfer settings = cs.readSettings();

		jtfTrfProvidedLastDate.setText(settings.trfProvidedLastDate);
		jtfTrfProvidedDays.setText(settings.trfProvidedDays);
		jtfTrfProvidedPerPlanPeriod.setText(settings.trfChargePerPlanPeriod);
		jchbTrfAutoReset.setSelected(settings.trfIsAutoReset);
		jchbTrfIsShowNumVal.setSelected(settings.trfIsShowNumVal);
	}

	/**
	 * Process and validate data from Settings View.
	 *
	 * @param settingsTransfer
	 * @return
	 */
	public boolean okClicked(SettingsTransfer settingsTransfer) {
		cs.setSettingsTransfer(settingsTransfer);
		ControllerSettings.ValidationResult res = cs.save();
		if ( !res.isSuccess() ) JOptionPane.showMessageDialog(this, res.getMessage(), "Ошибка ввода", JOptionPane.ERROR_MESSAGE);

		return res.isSuccess();
	}

	/**
	 * Class for transfer data from ViewSettings user changed to ControllerSettings.
	 */
	public class SettingsTransfer {
		public String trfProvidedLastDate;		// See {@link Util#DATE_PATTERN}
		public String trfProvidedDays;			// (Days)
		public String trfChargePerPlanPeriod;	// (MB)
		public boolean trfIsAutoReset;
		public boolean trfIsShowNumVal;
	}

}
