package view;

import controller.ConnState;
import model.LocalStorageI;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Main Window. App started from this Window.
 */
public class ViewApp extends JFrame implements ViewAppCallbackI {

	private static final boolean IS_LOG = true;

	private final Dimension windowSize = new Dimension(700, 150);
	private static final String TXT_TRAFFIC_REMAINS = "Остаток (МБ): ";
	private static final String TXT_MODEM_STATE = "Состояние: ";

	private JPanel jpMain;
	private JProgressBar jpbTrfAvailable;
	private JLabel jlTrfAv;
	private JLabel jlConnState;

	private LocalStorageI localStorage;

	public enum FontSize {
		SMALL(12), MEDIUM(18), LARGE(22);

		private int size;

		FontSize(int size) {
			this.size = size;
		}

		public int getVal() {
			return size;
		}
	}


	public ViewApp(String title, LocalStorageI localStorage) throws HeadlessException {
		super(title);
		this.localStorage = localStorage;

		jpMain = new JPanel();
		jpMain.setBounds(18, 18, 1, 1);
		jpMain.setLayout(new BoxLayout(jpMain, BoxLayout.Y_AXIS));

		registerShortKeysDialogsAndFrames();

		jpbTrfAvailable = new JProgressBar(0, 100);
//		jpbTrfAvailable.setValue(80);
		jpbTrfAvailable.setOrientation(SwingConstants.HORIZONTAL);
		jpbTrfAvailable.setForeground(Color.green);
		jpMain.add(jpbTrfAvailable);


		jlTrfAv = new JLabel(TXT_TRAFFIC_REMAINS);
		jpMain.add(jlTrfAv);

		jlConnState = new JLabel(TXT_MODEM_STATE);
		jpMain.add(jlConnState);

		this.add(jpMain);

		pack();
		setSize(windowSize);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	/**
	 * This dialogs or frames should be called from hot (short) keys from root (this) frame.
	 */
	private void registerShortKeysDialogsAndFrames() {
		// For open Settings model window
		ActionListener sl = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				showOptionsDialog();
			}
		};
		KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK, false);
		jpMain.registerKeyboardAction(sl, keyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
	}

	private void showOptionsDialog() {
		ViewSettings sDialog = new ViewSettings(this, localStorage);
		sDialog.pack();
		sDialog.setLocationRelativeTo(this);
		sDialog.setVisible(true);
	}


	@Override
	public void setConnectionState(ConnState connState) {
		jlConnState.setText(TXT_MODEM_STATE + connState.getState());
		jlConnState.setForeground(Color.decode(connState.getTextColor()));
	}

	@Override
	public void setTrfAvailableMB(int traffic) {
		jlTrfAv.setText(TXT_TRAFFIC_REMAINS + String.format("%,d", traffic));
	}

	@Override
	public void setTrfAvailablePercentage(int percentage) {
		jpbTrfAvailable.setValue(percentage);
	}

	@Override
	public void setFontSize(FontSize size) {
		jlTrfAv.setFont(new Font("Arial", Font.BOLD, size.getVal()));
		jlConnState.setFont(new Font("Arial", Font.BOLD, size.getVal()));
	}


	private static void log(String msg) {
		if(IS_LOG) System.out.println(ViewApp.class.getSimpleName() + ": " + msg);
	}
}
