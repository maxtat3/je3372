package app;

import controller.ControllerApp;
import model.LocalStorageAdapter;
import model.LocalStorageI;
import util.AppProperties;
import view.ViewApp;

import java.io.IOException;

/**
 * Created by max on 09.04.21.
 */
public class App {

	// TODO: 10.04.21 refactored to gradle.properties
	public static final String APP_NAME = "jE3372_v0.1";


	public static void main(String[] args) throws IOException {

		LocalStorageI localStorage = new LocalStorageAdapter();
		localStorage.load();

		ViewApp viewApp = new ViewApp(APP_NAME, localStorage);
		ControllerApp controllerApp = new ControllerApp(viewApp, localStorage);


		// http://java-online.ru/swing-layout.xhtml
//		javax.swing.SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				view.ViewApp viewApp = new view.ViewApp(APP_NAME);
//			}
//		});


	}


}
