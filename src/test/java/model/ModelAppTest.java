package model;

import model.ModelApp;
import org.junit.Assert;
import org.junit.Test;
import pojo.TrafficStatistics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by max on 23.07.21.
 */
public class ModelAppTest {

	private static final String APATH_API_MON_TRAFFIC_STAT = "src/test/resources/api_monitoring_traffic-statistics.xml";


	private InputStream readFileAsIS(String filePath) throws FileNotFoundException {
		File f = new File(filePath);
		if (f.isFile()) {
			return new FileInputStream(f);

		}
		return null;
	}


	@Test
	public void testRetrieveTrafficStatisticsFromModem() throws Exception {
		TrafficStatistics tStat = new ModelApp().retrieveTrafficStatistics();

		Assert.assertEquals(243360, tStat.getCurrentConnectTime());
		Assert.assertEquals(244020, tStat.getCurrentUpload());
		Assert.assertEquals(1135754, tStat.getCurrentDownload());
		Assert.assertEquals(10613008, tStat.getTotalConnectTime());
	}

	@Test
	public void testParseXML() throws Exception {
		TrafficStatistics tStat = new ModelApp().parseXML(readFileAsIS(APATH_API_MON_TRAFFIC_STAT));

		Assert.assertEquals(243360, tStat.getCurrentConnectTime());
		Assert.assertEquals(244020, tStat.getCurrentUpload());
		Assert.assertEquals(1135754, tStat.getCurrentDownload());
		Assert.assertEquals(10613008, tStat.getTotalConnectTime());
	}



}