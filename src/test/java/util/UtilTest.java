package util;

import model.LocalStorageAdapter;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;


public class UtilTest {

	//**********************************
	//	Test method convertBytesToMB
	//----------------------------------
	// passed 1 МB
	@Test
	public void testConvertBytesToMB_passed1MB() {
		Assert.assertEquals(1, Util.convertBytesToMB(1048576));
	}


	// passed = 0.476 MB
	@Test
	public void testConvertBytesToMB_passedLess0dot5MB() {
		Assert.assertEquals(0, Util.convertBytesToMB(500000));
	}

	// passed = 0.56 MB
	@Test
	public void testConvertBytesToMB_passedMore0dot5MB() {
		Assert.assertEquals(1, Util.convertBytesToMB(590000));
	}

	// passed = 1.0000038 MB
	@Test
	public void testConvertBytesToMB_passedMore1MB() {
		Assert.assertEquals(1, Util.convertBytesToMB(1048580));
	}


	// passed = 1.4 MB
	@Test
	public void testConvertBytesToMB_passed1dot4MB() {
		Assert.assertEquals(1, Util.convertBytesToMB(1468006));
	}

	// passed = 1.6 MB
	@Test
	public void testConvertBytesToMB_passed1dot6MB() {
		Assert.assertEquals(2, Util.convertBytesToMB(1677721));
	}
	//**********************************

	//**********************************
	//	Test method convertBytesToMB
	//----------------------------------
	@Test
	public void testConvertMBToBytes_passed1MB() {
		Assert.assertEquals(1048576, Util.convertMBToBytes(1));
	}

	@Test
	public void testConvertMBToBytes_passed155MB() {
		Assert.assertEquals(162529280, Util.convertMBToBytes(155));
	}

	@Test
	public void testConvertMBToBytes_passed5500MB() {
		Assert.assertEquals(57671680000L, Util.convertMBToBytes(55000));
	}

//	@Test
//	public void testConvertMBToBytes_passedMore0dot5MB() {
//		Assert.assertEquals(1, Util.convertMBToBytes(1));
//	}

//	@Test
//	public void testConvertMBToBytes_passed() {
//
//	}
//
//	@Test
//	public void testConvertMBToBytes_passed() {
//
//	}

	//	@Test
//	public void testConvertMBToBytes_passed() {
//
//	}

	//	@Test
//	public void testConvertMBToBytes_passed() {
//
//	}


	//**********************************

	//**********************************
	//	Test method getDifferenceDays
	//----------------------------------
	@Test
	public void testGetDifferenceDays_diff2days() throws Exception {
		long days = Util.getDifferenceDays("15 05 2021", "17 05 2021");
		Assert.assertEquals(2, days);
	}

	@Test
	public void testGetDifferenceDays_diff10days() throws Exception {
		long days = Util.getDifferenceDays("15 05 2021", "25 05 2021");
		Assert.assertEquals(10, days);
	}

	@Test
	public void testGetDifferenceDays_diff92days() throws Exception {
		long days = Util.getDifferenceDays("15 05 2021", "15 08 2021");
		Assert.assertEquals(92, days);
	}

	@Test
	public void testGetDifferenceDays_diff366days() throws Exception {
		long days = Util.getDifferenceDays("15 07 2015", "15 07 2016");
		Assert.assertEquals(366, days);
	}

	@Test
	public void testGetDifferenceDays_equalDates() throws Exception {
		long days = Util.getDifferenceDays("15 05 2021", "15 05 2021");
		Assert.assertEquals(0, days);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetDifferenceDays_wrong_d1Mored2() throws Exception {
		Util.getDifferenceDays("25 05 2021", "17 05 2021");
	}

	@Test(expected = ParseException.class)
	public void testGetDifferenceDays_wrong_date1IsIncorrectDashInDate() throws Exception {
		Util.getDifferenceDays("15-05-2021", "17 05 2021");
	}

	@Test(expected = ParseException.class)
	public void testGetDifferenceDays_wrong_date1IsIncorrectPassedText() throws Exception {
		Util.getDifferenceDays("abcd", "15 05 2021");
	}

	@Test(expected = ParseException.class)
	public void testGetDifferenceDays_wrong_bothDatesIsIncorrectPassedText() throws Exception {
		Util.getDifferenceDays("abcd", "abcde");
	}

	//**********************************


	//**********************************
	//	Test method addDays
	//----------------------------------
	@Test
	public void testAddDays_add0days() throws Exception {
		String newDate = Util.addDays("15 05 2021", 0);
		Assert.assertEquals("15 05 2021", newDate);
	}

	@Test
	public void testAddDays_add2days() throws Exception {
		String newDate = Util.addDays("15 05 2021", 2);
		Assert.assertEquals("17 05 2021", newDate);
	}

	@Test
	public void testAddDays_add15days() throws Exception {
		String newDate = Util.addDays("15 05 2021", 15);
		Assert.assertEquals("30 05 2021", newDate);
	}

	@Test
	public void testAddDays_add28days() throws Exception {
		String newDate = Util.addDays("15 05 2021", 28);
		Assert.assertEquals("12 06 2021", newDate);
	}

	@Test
	public void testAddDays_add35days() throws Exception {
		String newDate = Util.addDays("15 05 2021", 35);
		Assert.assertEquals("19 06 2021", newDate);
	}
	//**********************************


	//**********************************
	//	Test method getDifferenceDays
	//----------------------------------
	@Test
	public void testIsNumeric_passed0() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("0"));
	}

	@Test
	public void testIsNumeric_passed1() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("1"));
	}

	@Test
	public void testIsNumeric_passed15() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("15"));
	}

	@Test
	public void testIsNumeric_passed155000() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("155000"));
	}

	// Переданное число в 11,8 раз больше чем тип Int (2^32/2)
	@Test
	public void testIsNumeric_passedNUmberIn12TimesMorThanInt() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("50688300183"));
	}

	@Test
	public void testIsNumeric_passedMinus1() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("-1"));
	}

	@Test
	public void testIsNumeric_passedMinus18() throws Exception {
		Assert.assertEquals(true, Util.isNumeric("-18"));
	}


	@Test
	public void testBoolFlip() {
		boolean a = true;

		a = !a;
		System.out.println("a1 = " + a);

		a = !a;
		System.out.println("a2 = " + a);

		a = !a;
		System.out.println("a3 = " + a);

		a = !a;
		System.out.println("a4 = " + a);
	}


}