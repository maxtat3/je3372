package controller;

import model.LocalStorageAdapter;
import model.LocalStorageI;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.logging.Logger;


public class ControllerSettingsTest {

	@Test
	public void testCheckTrafficProvidedDays() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputNumericValue("5", "0", 0, 366);
		Assert.assertEquals(true, res.isSuccess());
		Assert.assertEquals(null, res.getMessage());
//		System.out.println(res.isSuccess() + " | " + res.getMessage());
	}

	@Test
	public void testTrafficChargePerPlanPeriod() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputNumericValue("56320", "1", 0, Integer.MAX_VALUE);
		Assert.assertEquals(true, res.isSuccess());
		Assert.assertEquals(null, res.getMessage());
//		System.out.println(res.isSuccess() + " | " + res.getMessage());
	}

	// ---

	@Test
	public void testCheckInputDateFormat_correctDate0() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01 07 2021", "0");
		Assert.assertEquals(true, res.isSuccess());
		Assert.assertEquals(null, res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_correctDate1() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("30 07 2020", "5");
		Assert.assertEquals(true, res.isSuccess());
		Assert.assertEquals(null, res.getMessage());
	}

	@Ignore
	@Test
	public void testCheckInputDateFormat_wrongDate_YY() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01 07 21", "0");    // TODO: 29.07.21 test with date [01 07 *21*] is passed !
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_wrongDate_dashDelimiter() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01-07-2021", "0");
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_wrongDate_slashDelimiter() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01/07/2021", "0");
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_wrongDate_dayOverrange() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("51 07 2021", "0");
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_wrongDate_monthOverrange() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01 17 2021", "0");
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_wrongDate_letterInDay() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01a 07 2021", "0");
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Ignore
	@Test
	public void testCheckInputDateFormat_wrongDate_letterYY() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("01 07 2021a", "0");	// TODO: 29.07.21 test with date [01 07 *21a*] is passed !
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	@Test
	public void testCheckInputDateFormat_wrongDate_textInsteadDate() throws Exception {
		ControllerSettings.ValidationResult res = new ControllerSettings().checkInputDateFormat("abcd", "0");
		Assert.assertEquals(false, res.isSuccess());
		Assert.assertEquals("В поле [0] дата должна соответствовать шаблону dd MM yyyy", res.getMessage());
	}

	// ---


	public static final Logger LOG = Logger.getLogger(ControllerSettingsTest.class.getName());

	@Test
	public void testLogger() throws IOException {
		LOG.info("abcd");
		LOG.info("def");
	}

}