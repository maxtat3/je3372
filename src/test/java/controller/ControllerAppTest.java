package controller;

import model.LocalStorageI;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;


public class ControllerAppTest {

	@Test
	public void testIsNewTrfProvidedPerPlanPeriod_correct_newTrfNotAv0() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("06 07 2021");
		storage.setTrfProvidedDays(28);
		String currDate = "01 08 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(false, controller.isNewTrfAvailable(currDate));
	}

	@Test
	public void testIsNewTrfProvidedPerPlanPeriod_correct_newTrfNotAv1() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("06 07 2021");
		storage.setTrfProvidedDays(28);
		String currDate = "02 08 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(false, controller.isNewTrfAvailable(currDate));
	}

	@Test
	public void testIsNewTrfProvidedPerPlanPeriod_correct_newTrfAv0() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("06 07 2021");
		storage.setTrfProvidedDays(28);
		String currDate = "03 08 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(true, controller.isNewTrfAvailable(currDate));
	}

	@Test
	public void testIsNewTrfProvidedPerPlanPeriod_correct_newTrfAv1() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("06 07 2021");
		storage.setTrfProvidedDays(28);
		String currDate = "15 08 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(true, controller.isNewTrfAvailable(currDate));
	}

	// TODO: 07.08.21 written test methods testIsNewTrfProvidedPerPlanPeriod ...
	/*
	@Test
	public void testIsNewTrfProvidedPerPlanPeriod() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("05 05 2021");
		storage.setTrfProvidedDays(55);
		String currDate = "15 07 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(false, controller.isNewTrfAvailable(currDate));
	}

	@Test
	public void testIsNewTrfProvidedPerPlanPeriod() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("05 05 2021");
		storage.setTrfProvidedDays(55);
		String currDate = "15 08 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(true, controller.isNewTrfAvailable(currDate));
	}

	@Test
	public void testIsNewTrfProvidedPerPlanPeriod() throws Exception {
		LocalStorageI storage = new LocalStorageMock();

		storage.setTrfProvidedLastDate("06 07 2022");	this date must be < than current
		storage.setTrfProvidedDays(30);
		String currDate = "01 08 2021";

		ControllerApp controller = new ControllerApp(storage);
		Assert.assertEquals(true, controller.isNewTrfAvailable(currDate));
	}
	 */



	private class LocalStorageMock implements LocalStorageI {
		private String trfProvidedLastDate;		// Property option
		private int trfProvidedDays;			// Property option
		private long trfConsumedBytes;			// Property option

		@Override
		public boolean load() throws IOException {
			return false;
		}

		@Override
		public void setTrfProvidedLastDate(String date) {
			trfProvidedLastDate = date;
		}

		@Override
		public String getTrfProvidedLastDate() {
			return trfProvidedLastDate;
		}

		@Override
		public void setTrfProvidedDays(int days) {
			trfProvidedDays = days;
		}

		@Override
		public void setTrfProvidedDays(String days) {

		}

		@Override
		public int getTrfProvidedDaysNum() {
			return trfProvidedDays;
		}

		@Override
		public String getTrfProvidedDaysStr() {
			return null;
		}

		@Override
		public void setTrfProvidedPerPlanPeriodBytes(long bytes) {

		}

		@Override
		public void setTrfProvidedPerPlanPeriodMB(String megaBytes) {

		}

		@Override
		public void setTrfProvidedPerPlanPeriodGB(String gigaBytes) {

		}

		@Override
		public long getTrfProvidedPerPlanPeriodBytes() {
			return 0;
		}

		@Override
		public int getTrfProvidedPerPlanPeriodMB() {
			return 0;
		}

		@Override
		public String getTrfProvidedPerPlanPeriodMBStr() {
			return null;
		}

		@Override
		public int getTrfProvidedPerPlanPeriodGB() {
			return 0;
		}

		@Override
		public String getTrfProvidedPerPlanPeriodGBStr() {
			return null;
		}

		@Override
		public void setTrfConsumedBytes(long bytes) {
			trfConsumedBytes = bytes;
		}

		@Override
		public long getTrfConsumedBytes() {
			return trfConsumedBytes;
		}

		@Override
		public void setTrfIsAutoReset(boolean isAutoReset) {

		}

		@Override
		public boolean getTrfIsAutoReset() {
			return false;
		}

		@Override
		public void setTrfAvlIsShowNumValue(boolean isShowNumValue) {

		}

		@Override
		public boolean getTrfAvlIsShowNumValue() {
			return false;
		}

		@Override
		public void save() throws IOException {

		}
	}


}