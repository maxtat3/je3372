<?php
	
	define("REFRECH_TRAFFIC_DATA", 1 );		// In seconds
	define("MB", "1048576");			// 1MB
	define("KB", "1024");				// 1kB

	$upl = 0;
	$dwl = 0;

	while (true) {
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
		<response>
			<CurrentConnectTime>0</CurrentConnectTime>
			<CurrentUpload>$upl</CurrentUpload>
			<CurrentDownload>$dwl</CurrentDownload>
			<CurrentDownloadRate>0</CurrentDownloadRate>
			<CurrentUploadRate>0</CurrentUploadRate>
			<TotalUpload>0</TotalUpload>
			<TotalDownload>0</TotalDownload>
			<TotalConnectTime>0</TotalConnectTime>
			<showtraffic>1</showtraffic>
		</response>";

		file_put_contents("/home/max/www/web-site/modemE3372/api/monitoring/traffic-statistics", $xml);
		$upl += 100 * KB;
		$dwl += MB;

		sleep(REFRECH_TRAFFIC_DATA);
	}

?>