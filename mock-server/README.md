# Тестирование отображения потребленного трафика

Для этого предусмотрен серверный скрипт *index.php* для эмуляции потребления трафика.    
Настроен по умолчанию таким образом, что каждую секунду генерируется:    

* Скачивание (Downloading) с сервера 1 МБ (1048576 Байт) данных и    
* Загрузка (Uploading) на сервер 100 КБ (102400 Байт) данных    

Если опрос модема программой *jE3372.jar* выполняется каждые 3с, тогда в сумме (Downloading + Uploading data) будет получено 3.3 МБ (3452928 Байт).    

Эмуляцию можно сделать на локальном сервере типа LAMPP. Для этого копируем каталог *modemE3372* в каталог сервера где размещаются сайты. Конечная точка для программы *jE3372.jar* из которой собирается статистика представляет собой XML файл *.../modemE3372/api/monitoring/traffic-statistics* содержимое которого динамически обновляется сервером.
Содержимое файла *traffic-statistics* следующее:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<response>
	<CurrentConnectTime>0</CurrentConnectTime>
	<CurrentUpload>0</CurrentUpload>
	<CurrentDownload>0</CurrentDownload>
	<CurrentDownloadRate>0</CurrentDownloadRate>
	<CurrentUploadRate>0</CurrentUploadRate>
	<TotalUpload>0</TotalUpload>
	<TotalDownload>0</TotalDownload>
	<TotalConnectTime>0</TotalConnectTime>
	<showtraffic>1</showtraffic>
</response>
```
Для старта процесса эмуляции, запускаем *index.php*. Значения в элементах CurrentUpload и CurrentDownload будут увеличиваться каждую секунду. 